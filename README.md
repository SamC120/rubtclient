RUBTClient
=====

Authors

  * Brian Kleszyk - 145004562 Designed the project and wrote the classes
  * Brian Ho - 139008780 Readme and debugging
  * Samuel Chang - 160004484 Wrote ServerFactory and ServerPeerHandler

Usage
------

  * Compile: `make`
  * Run: java RUBTClient Phase1.torrent

Program structure
-----

## Main classes

-----

#### RUBTClient

  * Nothing more than the main function. Calls upon the BtClient.BtClient to
    begin implementation.
  * Handles arguements and starts the BtClient class

#### BtClient.BtClient

  * The BtClient starts a torrent manager which manages the torrent file
  * A bridge between the peers and the actual file
  * Starts the torrent manager as a thread
  
#### BtClient.PeerException

  * Exception that would cause peer to close but not the client to close

#### BtClient.TorrentManager

 * A bridge between peers and their management and the torrent data file

#### BtClient.FileWriter

 * Verifies a piece to the file

#### BtClient.Handshake

 * Class with a static function that constructs a byte array of a handshake

#### BtClient.Message

 * Class with static functions that returns byte arrays based off on function arguements

#### BtClient.PeerFactory

 * Handles multiple peer handler threads, includes a queue of pieces that peers should download and provides mechanism for them to know which piece to download

#### BtClient.PeerHandler

 * Manges connection to peer, opens and closes connections and keeps state of connection

#### BtClient.ServerFactory

 * Not implemented, would manage incoming connections on a server port and based on connection, create a server peer handler to handle it

#### BtClient.ServerPeerHandler

 * Not implemented, handles would be server connections

#### BtClient.Download

 * Not used

#### BtClient.Key

 * Static variables pertaining values from tracker connection

#### BtClient.TrackerConnection

 * Manages tracker connection and can return a list of peers from the tracker

#### BtClient.TorrentData

  * Creates the file writer class as needed and handles pieces as they are committed, it will verify the checksum and pass it off to file writer if it's right

#### BtClient.Tracker

  * The BtClient.Tracker opens a raw connection to the tracker and returns a response
    from the tracker
    
#### BtClient.Downloader

  * Not used

## Smaller Classes

-----

 
#### BtClient.BtClientException

  * Exception class (modded off of BencodingException) to handle other 
    exceptions.

#### BtClient.RandomString

  * Creates a random string (`peer_id`)

#### BtClient.Peer

  * The peer object, to easily transfer all of the `peer_id` with the `peer_ip`
    and `port`


  
