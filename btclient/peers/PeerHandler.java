package btclient.peers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import btclient.PeerException;
import btclient.peers.Message;

class IncomingThread implements Runnable{
	volatile DataInputStream  in;
	PeerFactory peerFactory; 
	PeerHandler peerHandler;
	Lock socketLock;
	public IncomingThread(DataInputStream in, PeerFactory peerFactory, PeerHandler peerHandler, Lock socketLock){
		this.in = in;
		this.peerFactory = peerFactory;
		this.peerHandler = peerHandler;
		this.socketLock = socketLock;
	}

	@Override
	public void run() {
		try{
			while(!this.peerFactory.getTerminate()){
				byte[] msglength = new byte[4];
				System.out.println("Reading");

				this.socketLock.lock();
				try{
					this.in.readFully(msglength);
				}catch(Exception e){
					throw e;
				}finally{
					this.socketLock.unlock();
				}
				int msglengthint = Message.toInt(msglength);
				if(msglengthint > 0){
					byte[] message = new byte[msglengthint];
					this.socketLock.lock();
					try{
						this.in.readFully(message);
					}catch(Exception e){
						throw e;
					}finally{
						this.socketLock.unlock();
					}
					process(message);
				}
			}
		}catch(IOException ioe){
			System.err.println("IOException with input, exiting");
			this.peerHandler.close();
		}
	}
	
	private void process(byte[] message){
		byte messageId = message[0];
		switch(messageId){
			case 0://Choke
				this.peerHandler.setChoke(true);
				break;
			case 1://Unchoke
				this.peerHandler.setChoke(false);
				break;
			case 2://Interested
				break;
			case 3://Not-Interested
				break;
			case 4://Have
				break;
			case 5://Bitfield
				break;
			case 6://request
				break;
			case 7://piece
				int piece_index = Message.toInt(Arrays.copyOfRange(message,1,5));
				int piece_begin = Message.toInt(Arrays.copyOfRange(message,5,9));
				byte[] piece_block = Arrays.copyOfRange(message,9,message.length);
				this.peerFactory.commitPiece(piece_index, piece_begin, piece_block);
				break;
				
		}
	}
}

public class PeerHandler implements Runnable
{
	private Peer peer;
	private PeerFactory peerFactory;
	volatile private Socket peerSocket;
	volatile public DataInputStream in;
	volatile private DataOutputStream out;
	private boolean choke;
	public Lock socketLock;

	public PeerHandler(Peer peer, PeerFactory peerFactory) 
			throws PeerException
	{
		this.peer = peer;
		this.peerFactory = peerFactory;
		this.peerSocket = null;
		this.in = null;
		this.out = null;
		this.choke = true;
		this.socketLock = new ReentrantLock();
	}
	private void writeOut(byte[] full_message) 
			throws IOException
	{
		System.out.println("Writing:"+new String(full_message));
		this.socketLock.lock();
		try{
			this.out.write(full_message);
		}catch(Exception e){
			throw e;
		}finally{
			this.socketLock.unlock();
		}
		System.out.println("Finished");

	}
	private void grabPiece(int index, int begin, int length) 
			throws IOException
	{
		System.out.print("Try to download piece: ");
		System.out.println(index);
		while(this.choke){
			writeOut(Message.interested());
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {}
		}
		writeOut(Message.request(index, begin, length));
	}
	@Override
	public void run() {
		System.out.println("Establishing connection to "+new String(this.peer.peer_ip));
		try {
			handshake();
			System.out.println("Handshake complete");
		} catch (PeerException e2) {
			System.err.println("Exception with handshake, closing connection");
			this.close();
			return;
		}
		IncomingThread incoming = new IncomingThread(this.in, this.peerFactory, this, this.socketLock);
		Thread incomingThread = new Thread(incoming);
		incomingThread.start();
		while(!this.peerFactory.getTerminate() ){
			Integer[] pieces = this.peerFactory.getPiece();
			if(pieces == null){
				break;
			}
			try {
				grabPiece(pieces[0].intValue(),pieces[1].intValue(),pieces[2].intValue());
			} catch (IOException e1) {
				System.err.println("IOException with peer handler");
				this.close();
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
		}
		this.close();
		this.peer.active = false;
		System.out.println("Closing connection with "+new String(this.peer.peer_ip));
	}
	private void handshake() 
			throws PeerException
	{
		connect();
		try{
			writeOut(Handshake.handshake(this.peerFactory.getInfo_hash(), this.peerFactory.getPeerId()));
			if( in.readByte() != 19)
				throw new PeerException("Error with Handshake.");
			byte[] protocol = new byte[19];
			in.readFully(protocol);
			if( !Arrays.equals(protocol,"BitTorrent protocol".getBytes()) )
				throw new PeerException("Bad BitTorrnet Protocol");
			byte[] reserved = new byte[8];
			in.readFully(reserved);
	
			byte[] info_hash = new byte[20];
			in.readFully(info_hash);
				if( !Arrays.equals(this.peerFactory.getInfo_hash(),
							info_hash) )
					throw new PeerException("Bad Info_hash");		
			byte[] con_peer_id = new byte[20];
			in.readFully(con_peer_id);
	
			if( !Arrays.equals(this.peer.peer_id, con_peer_id) )
				throw new PeerException("Bad peer id");
		}catch( IOException ioe ){
			throw new PeerException("IOException error with handshake");
		}
		//System.out.println("Established connection to "+new String(this.peer.peer_ip));
	}
	private void connect() 
			throws PeerException
	{
		this.peerSocket = null;
		this.out = null;
		this.in = null;
		try{
			this.peerSocket = new Socket();
			this.peerSocket.connect(new InetSocketAddress(new String(this.peer.peer_ip), this.peer.port), 100);
			this.out = new DataOutputStream(this.peerSocket.getOutputStream());
			this.in = new DataInputStream(this.peerSocket.getInputStream());
		}catch(IOException ioe){
			throw new PeerException("IOException when connecting to peer.");
		}
	}
	public void close()
	{
		this.peer.active = false;
		if( this.in != null){
			try {
				this.in.close();
			} catch (IOException e) {
				System.err.println("Suppressed IOException with closing peer connection");
			}
		}
		if( this.out != null){
			try {
				this.out.close();
			} catch (IOException e) {
				System.err.println("Suppressed IOException with closing peer connection");
			}
		}
		if( this.peerSocket != null){
			try {
				this.peerSocket.close();
			} catch (IOException e) {
				System.err.println("Suppressed IOException with closing peer connection");
			}
		}
		
	}
	public void setChoke(boolean choke){
		this.choke = choke;
	}


}
