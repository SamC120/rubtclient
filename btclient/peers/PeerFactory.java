package btclient.peers;

//import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import btclient.BtClientException;
import btclient.PeerException;
import btclient.TorrentManager;
import btclient.peers.PeerHandler;
import btclient.tracker.TrackerConnection;



public class PeerFactory implements Runnable
{
	private volatile List<Peer> peers;
	//private volatile List<PeerHandler> peerHandlers;
	//private static final int max_peer_handlers = 5;
	private TrackerConnection trackerConnection;
	private TorrentManager tm;


	public PeerFactory(TrackerConnection trackerConnection, TorrentManager tm) 
			throws BtClientException
	{
		this.trackerConnection = trackerConnection;
		//this.peerHandlers = new ArrayList<PeerHandler>();
		this.tm = tm;
	}
	public void start() 
	{
		ExecutorService executor = Executors.newFixedThreadPool(1);
		executor.execute(this);
		System.out.println("PeerFactory set up");
	}
	public void run() 
	{
		try {
			this.peers = this.trackerConnection.getPeers();
		} catch (BtClientException e1) {
			e1.printStackTrace();
			this.tm.terminate = true;
			return;
		}
		ExecutorService executor = Executors.newFixedThreadPool(3);
		while(!this.tm.terminate){
			for(Peer peer:this.peers){
				if( !peer.active ){
					peer.active = true;
					try {
						executor.execute(new PeerHandler(peer,this));
					} catch (PeerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
		}
		System.out.println("PeerFactory shutting down");
	}
	public Integer[] getPiece(){
		return this.tm.getPiece();
	}
	public byte[] getInfo_hash(){
		return tm.getInfo_hash();

	}
	public boolean getTerminate() {
		return this.tm.terminate;
	}
	public boolean commitPiece(int index, int begin, byte[] bytes)
	{
		return this.tm.commitPiece(index, begin, bytes);
	}
	public byte[] getPeerId() {
		// TODO Auto-generated method stub
		return this.tm.getPeerId();
	}}
