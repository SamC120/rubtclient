package btclient.peers;

import java.nio.ByteBuffer;

public class Message
{
	public static byte[] keepAlive(){
		byte[] length_prefix = toByteArray(0);
		return length_prefix;
	}
	public static byte[] choke(){
		return getMessage(1, (byte)0);
	}
	public static byte[] unchoke(){
		return getMessage(1, (byte)1);
	}
	public static byte[] interested(){
		return getMessage(1, (byte)2);
	}
	public static byte[] notInterested(){
		return getMessage(1, (byte)3);
	}
	public static byte[] have(int piece){
		return getMessage(5, (byte)4, toByteArray(piece));
	}
	public static byte[] bitfield(byte[] payload){
		return getMessage(1+payload.length, (byte)5, payload);
	}
	public static byte[] request(int index, int begin, int length){
		byte[] payload = new byte[12];
		byte[] index_bytes = toByteArray(index);
		byte[] begin_bytes = toByteArray(begin);
		byte[] length_bytes=toByteArray(length);
		System.arraycopy(index_bytes, 0,
				payload, 0, index_bytes.length);
		System.arraycopy(begin_bytes, 0,
				payload, 4, begin_bytes.length);
		System.arraycopy(length_bytes, 0,
				payload, 8, length_bytes.length);
		return getMessage(13, (byte)6, payload);
	}
	public static byte[] piece(int index, int begin, byte[] block)
	{
		byte[] payload = new byte[8+block.length];
		byte[] index_bytes = toByteArray(index);
		byte[] begin_bytes = toByteArray(begin);
		System.arraycopy(index_bytes, 0,
				payload, 0, index_bytes.length);
		System.arraycopy(begin_bytes, 0,
				payload, 4, begin_bytes.length);
		System.arraycopy(block, 0,
				payload, 8, block.length);
		return getMessage(9+block.length, (byte)7, payload);
	}
	private static byte[] getMessage(int length_prefix, byte msg)
	{
		byte[] destination = new byte[5];
		byte[] length_prefix_bytes = toByteArray(length_prefix);
		System.arraycopy(length_prefix_bytes, 0, 
				destination, 0, length_prefix_bytes.length);
		destination[4] = msg;
		return destination;
	}
	private static byte[] getMessage(int length_prefix, byte msg, byte[] payload)
	{
		byte[] destination = new byte[5+payload.length];
		byte[] length_prefix_bytes = toByteArray(length_prefix);
		System.arraycopy(length_prefix_bytes, 0,
				destination, 0, length_prefix_bytes.length);
		destination[4] = msg;
		System.arraycopy(payload, 0, destination,
				5, payload.length);
		return destination;
	}
	private static byte[] toByteArray(int integer)
	{
		return ByteBuffer.allocate(4).putInt(integer).array();
	}
	public static int toInt(byte[] bytes)
	{
		ByteBuffer wrapped = ByteBuffer.wrap(bytes);
		return wrapped.getInt();
	}
}
