package btclient.peers;


public class Handshake {
	public static byte[] handshake(byte[] info_hash, byte[] peer_id){
		byte[] output = new byte[1+19+8+info_hash.length+peer_id.length];
		byte[] prot = {'B','i','t','T','o','r','r','e','n','t',' ','p','r','o','t','o','c','o','l'};

		for(int i=0; i<prot.length; i++){
			output[i+1] = prot[i];
		}
		for(int i=1+19; i< 1+19+8; i++){
			output[i] = 0x0;
		}
		for(int i=0; i<20; i++){
			output[i+1+19+8] = info_hash[i];
		}
		for(int i=0; i<20; i++){
			output[i+1+19+8+20] = peer_id[i];
		}
		output[0] = 0x13;

		//for(int i=0; i<output.length;i++){
		//	System.out.print(output[i]); System.out.print(", ");
		//}
		//System.out.println(new String(output));
		return output;
	}
}
