package btclient.peers;


public class Peer
{

	public final byte[] peer_id;
	public final byte[] peer_ip;
	public final Integer port;
	public boolean active;


	public Peer(byte[] peer_id, byte[] peer_ip, Integer port)
	{
		this.peer_id = peer_id;
		this.peer_ip = peer_ip;
		this.port = port;
		this.active = false;
	}

	public String getIp(){
		return new String(this.peer_ip) + 
			":" + 
			this.port.toString();
	}
	public String toString(boolean peer_id){
		return new String(this.peer_id) + " - " + 
			new String(this.peer_ip) + ":" + 
			this.port.toString();
	}
    public Peer clone() {
        try {
            return (Peer)super.clone();
        }
        catch (CloneNotSupportedException e) {
            // This should never happen
        }
		return null;
    }
}
