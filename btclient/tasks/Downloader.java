package BtClient;

import java.net.Socket;
import java.net.ServerSocket;
import java.net.SocketException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.lang.Thread;
import java.lang.InterruptedException;


import BtClient.TorrentData;
import BtClient.Tracker;
import BtClient.Peer;


/**
 * The Downloader will download the file.
 * It will communcate with the Tracker and TorrentData to complete the file.
 */
public class Downloader
{
	private boolean choked;
	public static final byte UNCHOKE = 1;
	public static final byte INTERESTED = 2;
	public static final byte HAVE = 5;
	public static final byte REQUEST = 6;
	public static final byte PIECE = 7;

	private TorrentData torrentData;
	private Tracker tracker;
	/**
	 * Initialize the Tracker
	 * @param String tracker_url
	 */
	public Downloader(
			TorrentData torrentData,
			Tracker tracker
	)
	{
		this.torrentData = torrentData;
		this.tracker = tracker;
	}
	/**
	 * Will download from the peers
	 */
	public void download()
		throws BtClientException
	{
		Peer peer = null;
		try{
			this.tracker.connect();
		}catch (BtClientException e){
			System.err.println("Unable to connect to tracker: "+
					e.toString());
			return;
		}
		if((peer = this.tracker.grab_peer()) == null){
			System.err.println("Unable to obtain peer");
		}
		System.out.println("Should now download from Rutgers peer: "+peer.toString(false));
		download_from_peer(peer);
		// torrentData.which_piece()
	}
	private void download_from_peer(Peer peer)
		throws BtClientException
	{
		Socket peerSocket = null;
		DataInputStream in = null;
		DataOutputStream out = null;
		try{
			peerSocket = new Socket(peer.peer_ip, peer.port);
			out = new DataOutputStream(peerSocket.getOutputStream());
			in = new DataInputStream(peerSocket.getInputStream());
		}catch(IOException ioe){
			throw new BtClientException("IOException: Could not connect to peer socket");
		}
		if(handshake(in, out, peer.peer_id)){
			System.out.println("Successful handshake with peer");
		}
		this.choked = true;
		int which_piece = -1;
		try{
			while((which_piece = this.torrentData.which_piece()) >= 0){
				//System.out.print("Looking for piece length: ");
				//System.out.println(this.torrentData.getPiece_length());
				if(this.choked){
					send_interested(out);
				}else{
					request_piece(which_piece, 0, this.torrentData.getPiece_length(), out);
					System.out.print(which_piece);System.out.print("|");
				}
				//System.out.print("Looking for piece: ");
				//System.out.println(which_piece);
				try{
					accept_input(in, out);
				}catch(IOException ioe){
					System.err.println("IOException:"+ioe.toString());
				}
				//try{
				//	Thread.sleep(1000);
				//}catch(InterruptedException e){}
			}
		}catch(IOException ioe){
			System.err.println("IOException caught");
		}

	}
	private void send_interested(DataOutputStream out)
		throws IOException
	{
		out.writeInt(1);
		out.writeByte(INTERESTED);
	}
	private void accept_input(DataInputStream in, DataOutputStream out)
		throws IOException
	{
		byte[] length = new byte[4];
		byte[] msg;
		byte msg_id;
		in.readFully(length);
		
		if(toInt(length) == 0){
			//keep alive
		}else{
			msg = new byte[toInt(length)];
			in.readFully(msg);
			msg_id = msg[0];
			//System.out.print("Received message: ");
			//System.out.println(msg_id);
			if(msg_id == UNCHOKE){
				this.choked = false;
			}else if(msg_id == PIECE){
				int piece_index = toInt(Arrays.copyOfRange(msg,1,5));
				int piece_begin = toInt(Arrays.copyOfRange(msg,5,9));
				byte[] piece_block = Arrays.copyOfRange(msg,9,msg.length);
				//System.out.print("Index: ");
				//System.out.println(piece_index);
				//System.out.print("Begin: ");
				//System.out.println(piece_begin);
				//System.out.print("Size: ");
				//System.out.println(piece_block.length);
				this.torrentData.commitPiece(piece_index, piece_begin, piece_block);
				//out.writeInt(5);
				//out.writeByte(HAVE);
				//out.writeInt(piece_index);
			}
		}
	}


	private void request_piece(int piece_index, int piece_begin, int piece_length, DataOutputStream out)
		throws IOException
	{
		out.writeInt(13);
		out.writeByte(REQUEST);
		out.writeInt(piece_index);
		out.writeInt(piece_begin);
		out.writeInt(piece_length);
	}

	private boolean handshake(DataInputStream in, DataOutputStream out, byte[] peer_id)
	{
		boolean result = true;
		try{
			out.writeByte(19);
			out.write("BitTorrent protocol".getBytes());
			out.write(new byte[8]);
			out.write(this.tracker.getInfo_hash().array());
			out.write(this.tracker.getPeer_id().getBytes());

			if( in.readByte() != 19)
				throw new BtClientException("Bad header received");
			byte[] protocol = new byte[19];
			in.readFully(protocol);
			if( !Arrays.equals(protocol,"BitTorrent protocol".getBytes()) )
				throw new BtClientException("Bad BitTorrnet Protocol");
			byte[] reserved = new byte[8];
			in.readFully(reserved);

			byte[] info_hash = new byte[20];
			in.readFully(info_hash);
				if( !Arrays.equals(tracker.getInfo_hash().array(),
							info_hash) )
					throw new BtClientException("Bad Info_hash");
			byte[] con_peer_id = new byte[20];
			in.readFully(con_peer_id);

			if( !Arrays.equals(peer_id,con_peer_id) )
				throw new BtClientException("Bad peer id");
		}catch(BtClientException bce){
			System.err.println("Error with headers communicating with peer");
			System.err.println(bce.toString());
			result = false;
		}catch(Exception e){
			System.err.println("Error connecting to peer. StackTrace:");
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	/**
	 * Download from a specific peer
	 * @param peer
	 * @param location
	 */
	public Byte download_piece(int location)
	{
		return null;
	}
	private byte[] toArray(int i)
	{
		return ByteBuffer.allocate(4).putInt(i).array();
	}
	private int toInt(byte[] bytes)
	{
		ByteBuffer wrapped = ByteBuffer.wrap(bytes);
		return wrapped.getInt();
	}
}
