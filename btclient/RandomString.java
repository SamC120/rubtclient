package btclient;

import java.util.Random;

public class RandomString
{
	private static final String AB = 
		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static Random rnd = new Random();
	private final String string;

	public RandomString(int len)
	{
		StringBuilder sb = new StringBuilder( len );
		for( int i = 0; i < len; i++ )
			sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		this.string = sb.toString();
	}
	public String toString()
	{
		return this.string;
	}
}
