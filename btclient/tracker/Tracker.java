package btclient.tracker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.HashMap;

import GivenTools.Bencoder2;
import GivenTools.BencodingException;
import btclient.BtClientException;
import btclient.TorrentManager;





/**
 * The tracker will communicate with the tracker.
 * It will open and send data from the tracker, and also close connections 
 * with the tracker.
 * It will also let the Downloader know which peers to communicate with.
 */
public class Tracker
{
	private URL announce_url;
	private byte[] info_hash;
	private TorrentManager torrentManager;

	/**
	 * Initialize the Tracker
	 * @param tracker_url
	 */
	public Tracker(
			URL announce_url,
			byte[] info_hash,

			TorrentManager torrentManager)
		throws BtClientException
	{
		this.announce_url = announce_url;
		this.info_hash = info_hash;
		this.torrentManager = torrentManager;
	}
	/**
	 * Connect to the tracker URL
	 * @throws BtClientException 
	 */
	public HashMap<ByteBuffer,Object> request() throws BtClientException

	{
			String url = announce_url.toString();
			if( !url.endsWith("?"))
				url += "?";
			byte[] bytes = this.info_hash;

			StringBuilder sb = new StringBuilder();
			for (byte b : bytes) {
				sb.append(String.format("%%%02X", b));
			}

			url += "info_hash=" + sb.toString() + 
				"&peer_id=" + this.torrentManager.peer_id + 
				"&port=" + 
					new Integer(this.torrentManager.serverPort).toString() + 
				"&uploaded=" + 
					new Integer(this.torrentManager.uploaded).toString() + 
				"&downloaded=" + 
					new Integer(this.torrentManager.downloaded).toString() + 
				"&left=" + 
					new Integer(this.torrentManager.getLeft()).toString();

			URL connect_url;
			try {
				connect_url = new URL(url);
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				throw new BtClientException("MalformedURLException on url:"+url);
			}
			byte[] response;
			try {
				response = getResponseAsByteArray(connect_url);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				throw new BtClientException("IOException with HTTP Response connecting to url:"+url);
			}
			
			Object temp;
			try {
				temp = Bencoder2.decode(response);
			} catch (BencodingException e) {
				throw new BtClientException("Error with Bencoded data.");
			}
			if( !(temp instanceof HashMap) ){
				throw new BtClientException("Bencoded data was a "+
						temp.getClass().toString()+
						" instead of the expected java.util.HashMap");
			}else{
				HashMap<ByteBuffer,Object> responseHM = 
					(HashMap<ByteBuffer,Object>)temp;
				return responseHM;
			}
	}
	private byte[] getResponseAsByteArray(URL url) throws IOException 

		//throws IOException 
	{
		System.out.println("Connecting to url: "+url.toString());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		InputStream is = null;
		try{
			is = url.openStream();
			byte[] byteChunk = new byte[2096];
			int n;

			while( (n = is.read(byteChunk)) > 0){
				baos.write(byteChunk,0,n);
			}
		}finally {
			if (is != null)
				is.close();
		}
		return baos.toByteArray();
	}
}


