package btclient.tracker;

import java.nio.ByteBuffer;

public class Key
{
	public static final ByteBuffer INTERVAL = ByteBuffer.wrap(new byte[] {
		'i', 'n', 't', 'e', 'r', 'v', 'a', 'l' });
	public final static ByteBuffer PEERS = ByteBuffer.wrap(new byte[]
		{ 'p', 'e', 'e', 'r', 's' });
	public final static ByteBuffer PEER_ID = ByteBuffer.wrap(new byte[]
		{ 'p','e','e','r',' ','i', 'd' });
	public final static ByteBuffer IP = ByteBuffer.wrap(new byte[]
		{ 'i', 'p' });
	public final static ByteBuffer PORT = ByteBuffer.wrap(new byte[]
		{ 'p','o','r','t' });

}
