package btclient.tracker;

import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import GivenTools.ToolKit;
import btclient.BtClientException;
import btclient.TorrentManager;
import btclient.peers.Peer;


public class TrackerConnection
{
	private Tracker tracker;
	private HashMap<ByteBuffer,Object> trackerHM;


	public TrackerConnection(
			byte[] info_hash,
			URL announce_url,
			TorrentManager torrentManager)
		throws BtClientException
	{
		this.tracker = new Tracker(
				announce_url,
				info_hash,
				torrentManager
				);
		this.trackerHM = null;
	}
	public List<Peer> getPeers() 
			throws BtClientException
	{
		if(trackerHM == null){
			trackerHM = this.tracker.request();
		}
		
		ArrayList peers = (ArrayList)trackerHM.get(Key.PEERS);
		List<Peer> peersList = new ArrayList<Peer>();
		byte[] RUBytes = ByteBuffer.wrap("-RU".getBytes()).array();
		byte[] AZBytes = ByteBuffer.wrap("-AZ".getBytes()).array();
		Peer peer = null;
		for( Object peerObj : peers ){
			HashMap<ByteBuffer,Object> peerHM = 
				(HashMap<ByteBuffer,Object>)peerObj;
			peer = null;
			peer = new Peer(
					((ByteBuffer) peerHM.get(Key.PEER_ID)).array(),
					((ByteBuffer) peerHM.get(Key.IP)).array(),
					(Integer) peerHM.get(Key.PORT)
					);
			
			if( peer != null && Arrays.equals(Arrays.copyOf(peer.peer_id, RUBytes.length), RUBytes) ||
					peer != null && Arrays.equals(Arrays.copyOf(peer.peer_id, AZBytes.length), AZBytes) ){
				//System.out.println("ADDING: "+new String(peer.peer_id)+" - "+new String(peer.peer_ip));
				peersList.add(peer);
			}else{
				//System.out.println("REJECTING: "+new String(peer.peer_id)+" - "+new String(peer.peer_ip));
			}
		}
		System.out.println("Got Peers:");
		for( Peer peer2:peersList ){
			System.out.println(peer2.toString(true));
		}
		return peersList;
	}
	
}
