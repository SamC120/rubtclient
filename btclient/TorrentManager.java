package btclient;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.Queue;

import btclient.data.TorrentData;
import btclient.peers.PeerFactory;
import btclient.tracker.TrackerConnection;
import GivenTools.TorrentInfo;


/**
 * The TorrentData object will verify data as it comes through from the peer.
 * It will act as 'shared-memory' with the Downloader. 
 * This will become important with multiple peers to add locks.
 */
public class TorrentManager implements Runnable
{
	private TorrentData torrentData;
	private PeerFactory peerFactory;
	public boolean terminate;
	public final String peer_id;
	public volatile int uploaded;
	public volatile int downloaded;
	public ServerSocket serverSocket;
	public int serverPort;
	public volatile TrackerConnection trackerConnection;
	private byte[] info_hash;
	
	private volatile Queue<Integer[]> pieces_to_get;
	private final int max_pieces_queuing = 5;

	/**
	 * Initialize the TorrentData object
	 * @param file_name 
	 * @param piece_length
	 * @param file_name
	 * @param file_length
	 * @param piece_hashes
	 * @throws BtClientException 
	 */
	public TorrentManager(
			TorrentInfo torrentInfo, String file_name
	) throws BtClientException
	{
		if(!claimPort())
			throw new BtClientException("Unable to claim a port.");
		this.terminate = false;
		this.info_hash = torrentInfo.info_hash.array();
		this.trackerConnection = new TrackerConnection(
				torrentInfo.info_hash.array(),

				torrentInfo.announce_url,
				this
				);
		this.peerFactory = new PeerFactory(this.trackerConnection, this);
		this.torrentData = new TorrentData(
				file_name, 
				torrentInfo.piece_length, 
				torrentInfo.file_length, 
				torrentInfo.piece_hashes);
		this.uploaded = 0;
		this.downloaded = 0;
		this.peer_id = (new RandomString(20)).toString();
		this.pieces_to_get = new LinkedList<Integer[]>();

	}
	public void run()
	{
		try {
			this.peerFactory.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addPiecesThread();
		
	}
	private void addPiecesThread(){
		Integer[] piece = null;
		while(!this.terminate){
			if(this.pieces_to_get.size() < this.max_pieces_queuing){
				piece = this.torrentData.getPiece();
				if(piece != null){
					this.pieces_to_get.add(piece);
				}else{
					this.terminate = true;
				}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {}
			}else{
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
			}
		}
	}
	public int getLeft(){
		return this.torrentData.left;
	}
	public boolean commitPiece(int index, int begin, byte[] bytes)
	{
		return this.torrentData.commitPiece(index, begin, bytes);
	}
	private boolean claimPort()
	{
		this.serverSocket = null;
		for(int i = 6881; i<= 6889;i++)
		{
			try {
				this.serverSocket = new ServerSocket(i);
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Claimed port "+(new Integer(
							i)).toString());
			this.serverPort = i;
			return true;
		}
		this.serverSocket = null;
		this.serverPort  = -1;
		return false;
	}
	public byte[] getInfo_hash(){
		return this.info_hash;
	}
	public Integer[] getPiece(){
		if(this.pieces_to_get.size() > 0){
			return this.pieces_to_get.poll();
		}
		return null;
	}
	public boolean getTerminate(){
		return this.terminate;
	}
	public byte[] getPeerId() {
		// TODO Auto-generated method stub
		return this.peer_id.getBytes();
	}

}
