package btclient;

import GivenTools.TorrentInfo;

/**
 * The BtClient will start the communications and download the torrent file.
 */
public class BtClient
{
	private TorrentInfo torrentInfo;
	private String file_name;
	/**
	 * Initialize the BtClient.
	 * Will be able to 
	 *		* Contact Tracker
	 *		* Initialize TorrentData Object
	 *		* Download using the Downloader
	 * @param torrentInfo
	 * @param file_name 
	 */
	public BtClient(TorrentInfo torrentInfo, String file_name)
	{
		this.torrentInfo = torrentInfo;
		this.file_name = file_name;
	}
	/**
	 * Set up required Tracker, Download the file, Keep track of it
	 * with the TorrentData object.
	 * Basically the 'main' of the BtClient
	 */
	public void run()
	{
		TorrentManager torrentManager = null;
		try {
			torrentManager = new TorrentManager(
					this.torrentInfo,
					this.file_name
			);
		} catch (BtClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		torrentManager.run();
		return;
	}
}
