package btclient;

/**
 * @author Robert Moore II
 * Modified by Brian Kleszyk
 *
 */
public final class BtClientException extends Exception
{
    /**
     * Automatically generated and unused.
     */
    private static final long serialVersionUID = -4829433031030292728L;
    
    /**
     * The message to display regarding the exception.
     */
    private final String message;
    
    /**
     * Creates a new BtClientException with a blank message.
     */
    public BtClientException()
    {
        this.message = null;
    }
    
    /**
     * Creates a new BtClientException with the message provided.
     * @param message the message to display to the user.
     */
    public BtClientException(final String message)
    {
        this.message = message;
    }
    
    /**
     * Returns a string containing the exception message specified during
     * creation.
     */
    @Override
    public final String toString()
    {
        return "BtClientException:\n"+(this.message == null ? "" : this.message);
    }
	public final String getMessage()
	{
		return (this.message == null ? "" : this.message);
	}

}
