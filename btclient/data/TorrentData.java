package btclient.data;


import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.locks.Lock;
/**
 * The TorrentData object will verify data as it comes through from the peer.
 * It will act as 'shared-memory' with the Downloader. 
 * This will become important with multiple peers to add locks.
 */
public class TorrentData
{


	private enum PieceStatus{
		INCOMPLETE, WORKING, COMPLETE
	}
	private final int piece_length;
	private final int file_length;
	private final ByteBuffer[] piece_hashes;

	private Lock commitLock;
	private volatile PieceStatus[] pieceStatus;
	private volatile FileWriter fileWriter;
	public volatile int left;

	/**
	 * Initialize the TorrentData object
	 * @param piece_length
	 * @param file_name
	 * @param file_length
<<<<<<< HEAD
	 * @param piece_hashes2
=======
	 * @param piece_hashes
>>>>>>> 78fb9f1870780712eb14df9e08c630b7e3775c0c
	 */
	public TorrentData(
			String file_name,
			int piece_length,
			int file_length,
			ByteBuffer[] piece_hashes

	)
	{
		this.piece_length = piece_length;
		this.file_length = file_length;
		this.piece_hashes = piece_hashes;
		this.pieceStatus = new PieceStatus[piece_hashes.length];

		for( int i=0; i<this.pieceStatus.length; i++){
			this.pieceStatus[i] = PieceStatus.INCOMPLETE;
		}
		this.fileWriter = new FileWriter(file_name);
		this.left = file_length;
	}

	public boolean commitPiece(int begin, int index, byte[] bytes)
	{
		commitLock.lock();
		if( pieceStatus[index] == PieceStatus.COMPLETE ){
			commitLock.unlock();
			return true;
		}

		byte[] hash = new byte[20];
		try{
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			hash = md.digest(bytes);
		}catch(NoSuchAlgorithmException nsae){
			System.err.println(
					"NoSuchAlgorithmException, unable to get SHA-1");
			commitLock.unlock();
			return false;
		}
		if( !Arrays.equals(this.piece_hashes[index].array(), hash) ){
			commitLock.unlock();
			return false;
		}
		if( writeToFile(index, begin, bytes) ){
			pieceStatus[index] = PieceStatus.COMPLETE;
			this.left -= bytes.length;
			commitLock.unlock();
			return true;
		}else{
			pieceStatus[index] = PieceStatus.INCOMPLETE;
			commitLock.unlock();
			return false;
		}
	}
	private boolean writeToFile(int index, int begin, byte[] bytes)
	{
		return this.fileWriter.write(index, begin, bytes);
	}

	public Integer[] getPiece() {
		for(int i=0; i< pieceStatus.length; i++){
			if(pieceStatus[i] == PieceStatus.INCOMPLETE){
				pieceStatus[i] = PieceStatus.WORKING;
				Integer[] returnInts = new Integer[3];
				returnInts[0] = i;
				returnInts[1] = 0;
				returnInts[2] = this.piece_length;
				return returnInts;
			}
		}
		for(int i=0; i< pieceStatus.length; i++){
			if(pieceStatus[i] == PieceStatus.WORKING){
				pieceStatus[i] = PieceStatus.WORKING;
				Integer[] returnInts = new Integer[3];
				returnInts[0] = i;
				returnInts[1] = 0;
				returnInts[2] = this.piece_length;
				return returnInts;
			}
		}
		System.out.println("No pieces left");
		return null;
	}
}
