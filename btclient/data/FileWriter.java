package btclient.data;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class FileWriter
{
	private RandomAccessFile randomAccessFile;
	public FileWriter(String filename)
	{
		try {
			this.randomAccessFile = new RandomAccessFile(filename, "rw");
		} catch (FileNotFoundException e) {
			System.err.println(e.toString());
		}
	}
	public boolean write(int index, int begin, byte[] bytes)
	{
		try {
			randomAccessFile.write(bytes, index, bytes.length);
		} catch (IOException e) {
			System.err.println("IOException when writing to file.");
			return false;
		}
		return true;
	}
}

