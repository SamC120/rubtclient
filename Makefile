JC=javac
JFLAGS=-cp . 

SOURCES=$(wildcard ./GivenTools/*.java) $(wildcard ./btclient/*.java) $(wildcard ./btclient/data/*.java) $(wildcard ./btclient/peers/*.java) $(wildcard ./btclient/tracker/*.java) ./RUBTClient.java
CLASSES=$(SOURCES:.java=.class)

all: compile

compile:
	$(JC) $(JFLAGS) $(SOURCES)

clean:
	$(RM) $(CLASSES)
