


import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import GivenTools.TorrentInfo;
import GivenTools.BencodingException;
import btclient.BtClient;

public class RUBTClient
{
	public static void main(String [] args)
		throws IOException
	{
		if(args.length != 2)
		{
			System.err.println("Must have 2 arguments");
			System.exit(1);
		}
		File file = new File(args[0]);
		String file_name = "";
		file_name = args[1];
		byte[] torrent_file_bytes = new byte[(int) file.length()];
		TorrentInfo torrentInfo = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(file);
			fileInputStream.read(torrent_file_bytes);
			torrentInfo = new TorrentInfo(torrent_file_bytes);
		} catch (FileNotFoundException e) {
			System.out.println("File Not Found.");
			e.printStackTrace();
		} catch (BencodingException e1) {
			System.err.println("Error reading File.");
			e1.printStackTrace();
		}
		BtClient btClient = new BtClient(
				torrentInfo,
				file_name
		);
		btClient.run();
	}

}
